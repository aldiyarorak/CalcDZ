﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            CalcTb.Text += b.Content;
        }

        private void EqualButton_Click(object sender, RoutedEventArgs e)
        {
            Result();
        }

        private void Result()
        {
            String op;
            int iOp = 0;
            if (CalcTb.Text.Contains("+"))
            {
                iOp = CalcTb.Text.IndexOf("+");
            }
            else if (CalcTb.Text.Contains("-"))
            {
                iOp = CalcTb.Text.IndexOf("-");
            }
            else if (CalcTb.Text.Contains("*"))
            {
                iOp = CalcTb.Text.IndexOf("*");
            }
            else if (CalcTb.Text.Contains("/"))
            {
                iOp = CalcTb.Text.IndexOf("/");
            }
            else
            {}

            op = CalcTb.Text.Substring(iOp, 1);
            double op1 = Convert.ToDouble(CalcTb.Text.Substring(0, iOp));
            double op2 = Convert.ToDouble(CalcTb.Text.Substring(iOp + 1, CalcTb.Text.Length - iOp - 1));

            if (op == "+")
            {
                CalcTb.Text += "=" + (op1 + op2);
            }
            else if (op == "-")
            {
                CalcTb.Text += "=" + (op1 - op2);
            }
            else if (op == "*")
            {
                CalcTb.Text += "=" + (op1 * op2);
            }
            else
            {
                CalcTb.Text += "=" + (op1 / op2);
            }
        }

        private void CButton_Click(object sender, RoutedEventArgs e)
        {
            CalcTb.Text = "";
        } 
    }
}
